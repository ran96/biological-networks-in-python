import unittest

import code_interactome
#import code_proteome

file_to_test = code_interactome.Interactome("fichier_test.txt", "uniprot_fichier_test.tab")
# File2 = Proteome.Proteome("Human_HighQuality.txt")
#file3 = code_proteome.Proteome("uniprot_fichier_test.tab")


class FunctionTest(unittest.TestCase):

    def test_count_vertices(self):
        count_vertices_type = type(code_interactome.Interactome.count_vertices(file_to_test))
        self.assertEqual(count_vertices_type, int)

    def test_count_vertices_2(self):
        result_int = 4
        self.assertEqual(result_int, code_interactome.Interactome.count_vertices(file_to_test))

    def test_count_edges(self):
        count_edges_type = type(code_interactome.Interactome.count_edges(file_to_test))
        self.assertEqual(count_edges_type, int)

    def test_count_edges_2(self):
        result_int = 2
        self.assertEqual(result_int, code_interactome.Interactome.count_edges(file_to_test))

    def test_get_degree(self):
        get_degree_type = type(code_interactome.Interactome.get_degree(file_to_test, 'INSR_HUMAN'))
        self.assertEqual(get_degree_type, int)

    def test_get_degree_2(self):
        result_int = 1
        self.assertEqual(result_int, code_interactome.Interactome.get_degree(file_to_test, 'INSR_HUMAN'))

    def test_get_all_degrees(self):
        get_all_degrees_type = type(code_interactome.Interactome.get_all_degrees(file_to_test))
        self.assertEqual(get_all_degrees_type, list)

    def test_get_all_degrees_2(self):
        result_list = [1, 1, 1, 1]
        self.assertEqual(result_list, code_interactome.Interactome.get_all_degrees(file_to_test))

    def test_get_max_degree(self):
        get_degree_type = type(code_interactome.Interactome.get_max_degree(file_to_test))
        self.assertEqual(get_degree_type, int)

    def test_get_max_degree_2(self):
        result_int = 1
        self.assertEqual(result_int, code_interactome.Interactome.get_max_degree(file_to_test))

    def test_get_average_degree(self):
        get_average_type = type(code_interactome.Interactome.get_average_degree(file_to_test))
        self.assertEqual(get_average_type, int)

    def test_get_average_degree_2(self):
        result_int = 1
        self.assertEqual(result_int, code_interactome.Interactome.get_average_degree(file_to_test))

    def test_count_degree(self):
        count_degree_type = type(code_interactome.Interactome.count_degree(file_to_test, 5))
        self.assertEqual(count_degree_type, int)

    def test_count_degree_2(self):
        result_int = 4
        self.assertEqual(result_int, code_interactome.Interactome.count_degree(file_to_test, 1))

    def test_cc_dict(self):
        result = code_interactome.Interactome.cc_dict(file_to_test)
        self.assertEqual(type(result), dict)

    def test_cc_dict_2(self):
        result_dict = {'INSR_HUMAN': ['1433E_HUMAN'], '1433B_HUMAN': ['1433G_HUMAN']}
        self.assertEqual(result_dict, code_interactome.Interactome.cc_dict(file_to_test))

    def test_dict_to_list(self):
        result = code_interactome.Interactome.dict_to_list(file_to_test)
        self.assertEqual(type(result), list)

    def test_dict_to_list_2(self):
        result_list = [['INSR_HUMAN', '1433E_HUMAN'], ['1433B_HUMAN', '1433G_HUMAN']]
        self.assertEqual(result_list, code_interactome.Interactome.dict_to_list(file_to_test))

    # def test_countcc(self):
    #   result = code_interactome.Interactome.countcc(file_to_test)
    #   self.assertEqual(type(result), 'NoneType')

    def test_extractcc(self):
        result = code_interactome.Interactome.extractcc(file_to_test, 'INSR_HUMAN')
        self.assertEqual(type(result), list)

    def test_extractcc_2(self):
        result_list = ['INSR_HUMAN', '1433E_HUMAN']
        self.assertEqual(result_list, code_interactome.Interactome.extractcc(file_to_test, 'INSR_HUMAN'))

    def test_extract_cc_number(self):
        result = code_interactome.Interactome.extract_cc_number(file_to_test, 'INSR_HUMAN')
        self.assertEqual(type(result), int)

    def test_extract_cc_number_2(self):
        result_int = 1
        self.assertEqual(result_int, code_interactome.Interactome.extract_cc_number(file_to_test, 'INSR_HUMAN'))

    def test_compute_cc(self):
        result = code_interactome.Interactome.compute_cc(file_to_test)
        self.assertEqual(type(result), list)

    def test_compute_cc_2(self):
        result_list = [1, 1, 2, 2]
        self.assertEqual(result_list, code_interactome.Interactome.compute_cc(file_to_test))

    def test_density(self):
        result = code_interactome.Interactome.density(file_to_test)
        self.assertEqual(type(result), float)

    def test_density_2(self):
        result_float = 0.33
        self.assertEqual(result_float, code_interactome.Interactome.density(file_to_test))

    # def test_clustering(self):
    #  result = code_interactome.Interactome.clustering(file_to_test, 'A')
    #   self.assertEqual(type(result), float)

    def test_xlink_uniprot(self):
        result = code_interactome.Interactome.xlink_uniprot(file_to_test, 'uniprot_fichier_test.tab')
        self.assertEqual(type(result), dict)

    def test_xlink_uniprot_2(self):
        result_dict = {'INSR_HUMAN': {'neighbours': ['1433E_HUMAN'], 'uniprotID': 'no uniprot id'},
                       '1433E_HUMAN': {'neighbours': ['INSR_HUMAN'], 'uniprotID': 'P62258'},
                       '1433B_HUMAN': {'neighbours': ['1433G_HUMAN'], 'uniprotID': 'P31946'},
                       '1433G_HUMAN': {'neighbours': ['1433B_HUMAN'], 'uniprotID': 'P61981'}}
        self.assertEqual(result_dict, code_interactome.Interactome.xlink_uniprot(file_to_test, 'uniprot_fichier_test'
                                                                                               '.tab'))

    def test_xlink_uniprot2(self):
        result_dict = {'1433E_HUMAN': {'neighbours': ['INSR_HUMAN'], 'uniprotID': 'P62258'},
                       '1433B_HUMAN': {'neighbours': ['1433G_HUMAN'], 'uniprotID': 'P31946'},
                       '1433G_HUMAN': {'neighbours': ['1433B_HUMAN'], 'uniprotID': 'P61981'}}
        self.assertEqual(result_dict, code_interactome.Interactome.xlink_uniprot2(file_to_test, 'uniprot_fichier_test'
                                                                                                '.tab'))

    def test_get_protein_domains(self):
        # a finir


unittest.main()

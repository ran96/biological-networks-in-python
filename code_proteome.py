# ################Week5####################
# la page pfam nous donne les domaines contenus dans notre protéine (start,
# end) ainsi que le lien vers la page uniprot de la proteine
# P06213 == identifiant uniprot de l'insuline humaine
# les domaines trouvés dans pfam :
# Recep L domain, Furin like, Recep L domain, Insulin TMD, PK TyrSER Thr
# les domaines trouvés sur Uniprot :
# Fibronectin type-III (3), Protein kinase
# les domaines different d'une database a une autre

import os


class Proteome():
    # Attributes
    prot_names_list = []
    ID_prot_dict = {}
    prot_ID_dict = {}

    # ajouter un dictionnaire qui est l'inverse du premier

    # Constructor
    def __init__(self, file):
        """
        constructor of the class proteome
        :param file:
        """
        self.ID_prot_dict = self.get_ID_protname_dict(file)
        self.prot_names_list = self.get_proteome_list()
        self.prot_ID_dict = self.get_protname_ID_dict(file)

    # methods

    def get_ID_protname_dict(self, file):
        """
        this method read a tabulated file and returns a dictionary containing as a key the uniprot id of the protein
        and as a value its name
        :param file:
        :return: dictionary
        """
        # read the file by using a wrapper
        with open(file, "r") as file_TextIOWrapper:
            # initialize the list
            proteome_dict = {}
            # skip the first line that contains the header
            file_TextIOWrapper.readline()
            # loop for to browse the lines of the file
            for line_str in file_TextIOWrapper:
                # separate the line into a list containing as elements each field of the line which are separated by
                # tabs
                fields_list = line_str.split("\t")
                # add the field[1] (identifiant uniprot) to the dictionary and its associated value (field[2]=name of
                # the protein)
                if fields_list[1] not in proteome_dict.keys():
                    proteome_dict[fields_list[1]] = fields_list[2]
        return proteome_dict

    def get_protname_ID_dict(self, file):
        """
        this method inverse the uniprot_protname dictionary and return a protname_uniprot dict
        :return: dictionary
        """
        # initialize the dict
        protname_uniprot_dict = {}
        # store the attribute in a variable
        uniprot_prot_dict = self.get_ID_protname_dict(file)
        # inverse the key value and add them to the new dictionary
        for key, value in uniprot_prot_dict.items():
            protname_uniprot_dict[value] = key
        return protname_uniprot_dict

    def get_proteome_list(self):
        """
        this method gives a list of the proteins contained in the file of proteome downloaded from uniprot
        :return: list
        """
        proteome_list = []
        for key in self.prot_ID_dict.keys():
            proteome_list.append(key)
        return proteome_list


file_to_test = Proteome("uniprot_fichier_test.tab")
# print(file_to_test.prot_ID_dict)
# print(file_to_test.ID_prot_dict)
# print(file_to_test.prot_names_list)


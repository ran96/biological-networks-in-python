import pickle
import self
from typing import Tuple
import matplotlib.pyplot as plt


class DomainGraph:
    # Attributes
    self.neigh_id_dom_graph_dict = {}
    self.prot_list = []
    self.dom_list = []
    self.dom_graph = {}
    self.edges = []
    self.nb_vertices = 0

    # Constructor
    def __init__(self):
        self.neigh_id_dom_graph_dict = self.get_final_dict()
        self.prot_list = self.get_prot_list()
        self.dom_list = self.ls_domains()
        self.dom_graph = self.generate_cooccurrence_graph()
        self.edges = self.get_edges()
        self.nb_vertices = len(self.get_vertices())

    # Methods
    #### dict final dans interactome = prot_nei_id_dom_dict.pkl
    def get_final_dict(self):
        """
        This method retrieve the final dict containing the domains by using the file created with pickle in the class Intercatome
        :return: dictionary
        """
        # use serialization to get the general dict (neighbours, uniprotid, domains) of our graph
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
        return final_dict

    def get_prot_list(self):
        """
        this method gets the list of proteins present in the graph
        :return:
        """
        prot_list = []
        for prot in self.neigh_id_dom_graph_dict.keys():
            prot_list.append(prot)
        return prot_list

    def ls_domains(self):
        """
        retrieve the list of non redundant domains
        :return: list
        """
        # initialize the list of domains
        non_redundant_domains_list = []
        for prot in self.prot_list:
            for dom in self.neigh_id_dom_graph_dict[prot]['domains']:
                if dom not in non_redundant_domains_list:
                    non_redundant_domains_list.append(dom)
        return non_redundant_domains_list

    def co_occurrence(self, dom_x, dom_y):
        """
        this method counts the number of occurrences of the two domains given as parameters in our graph
        :param dom_x:
        :param dom_y:
        :return: int (number of occurrences)
        """
        nb_occurrences_int = 0
        # for elem in self.get_unique_domains_per_prot():
        for prot in self.prot_list:
            # check if both domains are present in the protein
            if dom_x != dom_y:
                if dom_x in self.neigh_id_dom_graph_dict[prot]['domains'] and dom_y in \
                        self.neigh_id_dom_graph_dict[prot]['domains']:
                    nb_occurrences_int += 1
        return nb_occurrences_int

    ## to help with the co_occurence graph function ##
    def get_unique_domains_per_prot(self):
        """
        Get the list of unique domains for each protein containing more than one dom in order to use it for
        co-occurrence method
        :return: list of unique domains per prot
        """
        dom_list = []
        for prot in self.neigh_id_dom_graph_dict.keys():
            if len(self.neigh_id_dom_graph_dict[prot]['domains']) > 1:
                prot_dom_unique_list = list(set(self.neigh_id_dom_graph_dict[prot]['domains']))
                if len(prot_dom_unique_list) > 1:
                    dom_list.append(prot_dom_unique_list)
        with open("unique_domains_per_prot.pkl", "wb") as pklFile:
            pickle.dump(dom_list, pklFile)
        return dom_list

    def generate_cooccurrence_graph(self):
        """
        Displays a dictionary having as key a domain and as value a list of domains that occur with it in
        the same protein.
        :return: dictionary
        """
        domain_graph_dict = {}
        # retrieve the domain's list of each protein
        for prot in self.neigh_id_dom_graph_dict.keys():
            dom_list = self.neigh_id_dom_graph_dict[prot]['domains']
            # make sure that the protein has more than one dom
            if len(dom_list) > 1:
                # browse the domain's list and add each one as key to the dict and the others as a value (co_occurrence)
                for dom1 in dom_list:
                    for dom2 in dom_list:
                        if dom1 not in domain_graph_dict.keys() and dom1 != dom2:
                            domain_graph_dict[dom1] = []
                        if dom1 != dom2 and dom2 not in domain_graph_dict[dom1]:
                            domain_graph_dict[dom1].append(dom2)
        with open("domain_graph_dict.pkl", "wb") as pklFile:
            pickle.dump(domain_graph_dict, pklFile)
        return domain_graph_dict  # new_instance

    def get_vertices(self):
        """
        Retrieve the vertices (domains) of our graph
        :return: list of domains
        """
        vertices_list = []
        for dom in self.dom_graph.keys():
            if dom not in vertices_list:
                vertices_list.append(dom)
        return vertices_list

    def get_edges(self):
        """
        this method displays a list containing the edges of the domain's graph
        :return: list of tuples
        """
        # initialize the list of interactant
        edges_list = []
        # add tuples containing a domain and its "neighbour"
        for dom in self.dom_graph:
            for neighbour in self.dom_graph[dom]:
                if (dom, neighbour) not in edges_list and (neighbour, dom) not in edges_list:
                    edge = tuple([dom, neighbour])
                    edges_list.append(edge)
        return edges_list

    def get_edges_graph(self, graph):
        """
        Return the list of edges of the graph given in parameter
        :param graph: the graph of which we want to collect the edges
        :return: list
        """
        # initialize the list of interactant
        edges_list = []
        # add tuples containing a domain and its "neighbour"
        for dom in graph.keys():
            for neighbour in graph[dom]:
                if (dom, neighbour) not in edges_list and (neighbour, dom) not in edges_list:
                    edge = tuple([dom, neighbour])
                    edges_list.append(edge)
        return edges_list

    def domain_graph_density(self):
        """
        Compute the density of the domain graph
        :return: int: density
        """
        # computing the density
        num = int(2 * len(self.edges))
        den = int(self.nb_vertices * (self.nb_vertices - 1))
        try:
            dens = num / den
        except ZeroDivisionError:
            dens = 0
        return dens

    def nb_of_neigh_dom_dict(self):
        """
        Displays a dict containing the domain as a key and the number of its neighbours as a value
        :return: dictionary
        """
        # initialize the dictionary which will contain a domain as a key and the number of neighbours as a value
        dom_nb_neigh_dict = {}
        # fill the dict with the domains and the nb of neighbours
        for dom in self.dom_graph.keys():
            if dom not in dom_nb_neigh_dict.keys():
                dom_nb_neigh_dict[dom] = len(self.dom_graph[dom])
        return dom_nb_neigh_dict

    def get_top_ten_dom(self):
        """
        Get the top 10 domains with the largest number of neighbors
        :return: list
        """
        # load the graph of domains and the number of neighbours
        dom_nb_neigh_dict = self.nb_of_neigh_dom_dict()
        # from collections import OrderedDict
        # dom_nb_neigh_dict_sorted = OrderedDict(sorted(dom_nb_neigh_dict.items(), key=lambda x: x[1]))
        # sort the dict and retrieve the ten domains with the largest number of neighbours
        dom_nb_neigh_dict_sorted = sorted(dom_nb_neigh_dict.items(), key=lambda x: x[1], reverse=True)[:10]
        domains_list = []
        # fill the list with the domains
        for elem in dom_nb_neigh_dict_sorted:
            domains_list.append(elem[0])
        return domains_list

    def get_flop_ten_dom(self):
        """
        Get the 10 domains with the lowest number of neighbors
        :return: list
        """
        # load the graph of domains and nb of their neighbours
        dom_nb_neigh_dict = self.nb_of_neigh_dom_dict()
        # sort the dict and retrieve the ten domains with the lowest number of neighbours
        dom_nb_neigh_dict_sorted = sorted(dom_nb_neigh_dict.items(), key=lambda x: x[1])[:10]
        domains_list = []
        # fill the list with the domains
        for elem in dom_nb_neigh_dict_sorted:
            couple = (elem[0], elem[1])
            domains_list.append(couple)
            # domains_list.append(elem[0])
        return domains_list

    def dom_frequencies(self):
        """
        this method creates a dict containing each dom of our graph and it's occurrence in all the prot of the graph
        :return: dictionary
        """
        # initialize the new dict of frequencies
        frequencies_dict = {}
        # add each domain as a key to the new dict
        for dom in self.dom_graph.keys():
            frequencies_dict[dom] = 0
            # count the number of times the dom is present in the proteins
            for prot in self.neigh_id_dom_graph_dict.keys():
                if dom in self.neigh_id_dom_graph_dict[prot]['domains']:
                    frequencies_dict[dom] += 1
        return frequencies_dict

    def most_freq_dom_with_more_neigh(self):
        """
        this method check if the domains with more neighbours are the most frequent in our graph
        :return: boolean
        """
        # retrieve the dict of frequencies
        freq_dict = self.dom_frequencies()
        # sort the dict in order the retrieve the 10 more frequent domains
        freq_lict_sorted = sorted(freq_dict.items(), key=lambda x: x[1], reverse=True)[:10]
        final_freq_list = []
        # retrieve the most frequent domains
        for elem in freq_lict_sorted:
            final_freq_list.append(elem[0])
        # check if the top 10 dom are the most frequent
        top_ten_list = self.get_top_ten_dom()
        # initialize a list to store the most frequent domains with the most neighbours
        most_freq_most_neig_list = []
        # browse both lists to retrieve the domains
        if len(top_ten_list) == len(final_freq_list):
            for dom1 in top_ten_list:
                for dom2 in final_freq_list:
                    if dom1 == dom2:
                        most_freq_most_neig_list.append(dom1)
        return most_freq_most_neig_list

    def co_occurrence_one_dom(self):
        """
        Gives the number of domains that occur more than once in a protein
        :return: int number of domains
        """
        nb_of_dom_int = 0
        # initialize a list to have the domains that occur more than once in a protein
        dom_list = []
        for prot in self.neigh_id_dom_graph_dict.keys():
            from collections import Counter
            # count the occurrence of domains with counter
            occ_dom = Counter(self.neigh_id_dom_graph_dict[prot]['domains'])
            # check the domains that have a value more than 1
            for key, val in occ_dom.items():
                if val > 1:
                    nb_of_dom_int += 1
                    dom_list.append(key)
        return nb_of_dom_int, dom_list

    def generate_cooccurrence_graph_np(self, n):
        """
        Create a new instance with new domain graph containing the domains that occur in n or more than n proteins
        :return: new object DomainGraph
        """
        # initialize the new graph
        domain_graph_dict_np = {}
        # create a new instance
        new_object = DomainGraph()
        # add each domain as a key to the dict
        # count = 0
        for dom1 in self.dom_graph.keys():
            # count += 1
            # print(dom1, count)
            for dom2 in self.dom_graph[dom1]:
                # check that the domains are diff and they occur in more than n proteins
                if dom1 != dom2 and self.co_occurrence(dom1, dom2) >= n:
                    # fill the dict with the domains
                    if dom1 not in domain_graph_dict_np.keys():
                        domain_graph_dict_np[dom1] = []
                    if dom2 not in domain_graph_dict_np[dom1]:
                        domain_graph_dict_np[dom1].append(dom2)
        # change the attribute dom_grah with the new sub-graph for the new instance
        new_object.dom_graph = domain_graph_dict_np
        # list of domains to store the domains that occur at least in n proteins
        dom_list = []
        for dom in domain_graph_dict_np.keys():
            dom_list.append(dom)
        new_object.dom_list = dom_list
        # get edges of the new instance for the next method
        new_object.edges = new_object.get_edges_graph(domain_graph_dict_np)
        # get the number of the vertices of the new graph and assign it to the new instance
        new_object.nb_vertices = len(new_object.dom_list)
        # store the new sub graph using pickle
        with open("domain_sub_graph_dict_np.pkl", "wb") as pklFile:
            pickle.dump(domain_graph_dict_np, pklFile)
        return new_object  # , domain_graph_dict_np  # revoiiiiiiiiiiiiiir#####

    def plot_generate_cooccurrence_graph_np(self):
        """
        Displays a histogram showing the relationship between the density of the graph and the threshold n
        """
        # initialize the list of abcisse and y-axis for the plot
        abcisse_list = []
        ordonne_list = []
        # fill the lists with values till 10 for abcisse and the density for each value of abcisse
        for i in range(0, 11):
            abcisse_list.append(i)
            new_instance = self.generate_cooccurrence_graph_np(i)
            density_float = new_instance.domain_graph_density()
            ordonne_list.append(density_float)
        # plot the histogram
        plt.bar(abcisse_list, ordonne_list)
        plt.xlabel('N')
        plt.ylabel('Density')
        plt.show()

    def generate_cooccurrence_graph_n(self, n):  # i can divide this function into 2
        """
        Create new instance with a graph containing only the domains that occur more than n times in a protein
        :param n:
        :return: object DomainGraph
        """
        # initialize the new graph
        domain_graph_dict_np = {}
        # create a new instance
        new_instance = DomainGraph()
        #### First get the occurrence of both domains ####
        # browse the list of domains for each protein
        for prot in self.neigh_id_dom_graph_dict.keys():
            # count the occurrence of both dom in the list of dom
            prot_dom_list = self.neigh_id_dom_graph_dict[prot]['domains']
            if len(prot_dom_list) > 1:
                # browse the list of domains
                for dom1 in prot_dom_list:
                    for dom2 in prot_dom_list:
                        # check that the domains are different
                        if dom1 != dom2:
                            # the number of occurrence of both dom is the biggest number, if they're equal the nb of
                            # occurrence remains the same
                            nb_occ_int = max(prot_dom_list.count(dom1), prot_dom_list.count(dom2))
                            ###### Then check if the occurrence is greater of equal to the argument n #####
                            if nb_occ_int >= n:
                                # fill the dict with the domain as key
                                if dom1 not in domain_graph_dict_np.keys():
                                    domain_graph_dict_np[dom1] = []
                                """if dom2 not in domain_graph_dict_np.keys():
                                    domain_graph_dict_np[dom2] = []"""
                                # add each domain to the list of the other
                                if dom2 not in domain_graph_dict_np[dom1]:
                                    domain_graph_dict_np[dom1].append(dom2)
                                """if dom1 not in domain_graph_dict_np[dom2]:
                                    domain_graph_dict_np[dom2].append(dom1)"""
        # Determine the attributes of the new instance
        new_instance.dom_graph = domain_graph_dict_np
        dom_list = []
        for dom in domain_graph_dict_np.keys():
            dom_list.append(dom)
        new_instance.dom_list = dom_list
        new_instance.edges = self.get_edges_graph(domain_graph_dict_np)
        new_instance.nb_vertices = len(new_instance.dom_list)
        return new_instance, domain_graph_dict_np

    def weighted_cooccurrence_graph(self):
        """
        Create a new instance with a weighted co_occurrence graph
        :return: new object DomanGraph
        """
        # create the new object
        new_obj = DomainGraph()
        wei_occ_dict = {}
        # browse the list of domains
        for dom1 in self.dom_graph.keys():
            for dom2 in self.dom_graph[dom1]:
                # check that the domains occur at least in one protein
                # if self.co_occurrence(dom1, dom2) != 0:
                co_occu_dom1_dom2_int = self.co_occurrence(dom1, dom2)
                # create a tuple containing the name of dom and the occurrence
                weight_dom2_tuple = tuple([dom2, co_occu_dom1_dom2_int])
                # fill the dict with the domains and their neighbours with the weight of the edge
                if dom1 not in wei_occ_dict.keys():
                    wei_occ_dict[dom1] = []
                if weight_dom2_tuple not in wei_occ_dict[dom1]:
                    wei_occ_dict[dom1].append(weight_dom2_tuple)
        with open("weighted_dict.pkl", "wb") as pklFile:
            pickle.dump(wei_occ_dict, pklFile)
        # Define the attributes of the new instance
        new_obj.dom_graph = wei_occ_dict
        dom_list = []
        for dom in wei_occ_dict.keys():
            dom_list.append(dom)
        new_obj.dom_list = dom_list
        new_obj.edges = self.get_edges_graph(wei_occ_dict)
        new_obj.nb_vertices = len(new_obj.dom_list)
        return new_obj  # , wei_occ_dict

    def graph_threshold(self, k):
        """
        Modify the graph dom by deleting the edges that weight less than k and get a subgraph
        :param k: threshold
        """
        new_obj = DomainGraph()
        # retrieve the graph
        instance = self.weighted_cooccurrence_graph()
        # initialize the new dict
        new_dict = {}
        for dom in instance.dom_graph.keys():
            # add the domain to the dict
            new_dict[dom] = []
            # check if the weight is less than k
            for couple in instance.dom_graph[dom]:
                print(instance.dom_graph[dom])
                if int(couple[1]) > int(k):
                    # add the domains that have a co-occurrence greater than k
                    new_dict[dom].append(couple)
                # else:
                # del new_dict[dom]
        # clean the dict by removing the domains with 0 value
        for domain in new_dict.copy():
            if len(new_dict[domain]) == 0:
                del new_dict[domain]
        # Define the attributes of the new instance
        new_obj.dom_graph = new_dict
        dom_list = []
        for dom in new_dict.keys():
            dom_list.append(dom)
        new_obj.dom_list = dom_list
        new_obj.edges = self.get_edges_graph(new_dict)
        new_obj.nb_vertices = len(new_obj.dom_list)
        return new_obj

    def export_graph(self, graph):
        """
        Creates a new csv file containing in each line couple of domains and the weight of their intercation
        :param graph: the graph to export as a csv file
        """
        import csv
        with open("graphe_exporte_k_10.csv", 'w') as file_graph:
            spamwriter = csv.writer(file_graph)
            # browse the dict to add the domains and the weight of their interaction
            for dom in graph.keys():
                for edge in graph[dom]:
                    spamwriter.writerow([dom, edge[0], edge[1]])
        return



test_file = DomainGraph()
# print('graph')
# print(len(test_file.neigh_id_dom_graph_dict))
# print("dom finaux")
# print(len(test_file.dom_graph.keys()))
# print("dom initiaux")
# print(len(test_file.dom_list))
# print(test_file.get_unique_domains_per_prot())
# print(test_file.generate_cooccurrence_graph_np(10))
# print(test_file.co_occurrence_list())
# print(test_file.plot_generate_cooccurrence_graph_np())
# print(test_file.generate_cooccurrence_graph_n(6))
# print(test_file.weighted_cooccurrence_graph())
# print(test_file.get_top_ten_dom())
# print(test_file.most_freq_dom_with_more_neigh())
# new_instance = test_file.graph_threshold(10)
# print(test_file.export_graph(new_instance.dom_graph))
# print(test_file.most_freq_dom_with_more_neigh())
print(test_file.plot_generate_cooccurrence_graph_np())
# print(test_file.generate_cooccurrence_graph())
# print(test_file.graph_threshold(10))
# new_instance = test_file.weighted_cooccurrence_graph()
# print(test_file.export_graph(new_instance.dom_graph))
# print("aretes")
# print(len(test_file.edges))
# print("vertices en len de dom-list initiaux")
# print(test_file.nb_vertices)
# print(test_file.domain_graph_density())
# print("vertices finaux")
# print(len(test_file.get_vertices()))
# print(test_file.dom_graph)
# print(test_file.generate_cooccurrence_graph_np(10)) #####revoir cette méthode de np car ça prend trop de temps
# dic = test_file.weighted_cooccurrence_graph() #####revoir cette méthode aussi qui prend bcp de temps
# print(dic.dom_graph)
# print(dic.dom_graph)
# print(test_file.dom_list)
# print(test_file.edges)
# print(test_file.nb_vertices)
# print(test_file.co_occurrence_one_dom())
# print(test_file.co_occurrence('Recep_L_domain', 'Recep_L_domain'))
# print(test_file.generate_cooccurrence_graph())
# print(test_file.domain_graph_density())
# print(test_file.nb_of_neigh_dom_dict())
# print(test_file.get_top_ten_dom())
# print(test_file.get_flop_ten_dom())
# print(test_file.generate_cooccurrence_graph_np(2))
# new_instance = test_file.weighted_cooccurrence_graph()
# objet = test_file.weighted_cooccurrence_graph()
# print(test_file.generate_cooccurrence_graph())
# print(new_instance.dom_list)
# print(new_instance.nb_vertices)
# print(new_instance.dom_graph)
# print(test_file.graph_threshold(2))
# print(test_file.graph_threshold(1))

# print(test_file.plot_generate_cooccurrence_graph_np())
# print(test_file.generate_cooccurrence_graph_n(2))
# print(test_file.generate_cooccurrence_graph_np(1))
# print(new_instance.weighted_cooccurrence_graph())

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 19 07:37:23 2020

@author: vivian
"""
import unittest

import TP1_Read_interaction_Graphe





class read_interaction_file(unittest.TestCase):

    def test_read_interac_dict(self):
        """This test allows you to see how read_interaction_file_dict works."""

        file = "file_to_clean.txt"
        # we store our output result in a variable d
        d = TP1_Read_interaction_Graphe.read_interaction_file_dict(file)
        # we store the type of our output variable
        t = type(d)
        # the output type is expected to be equal to a dict
        self.assertEqual(t, dict)
        # we know that A is a key in the toy file we store it
        a = 'A'
        # We ask if A is a key in our output file.
        self.assertTrue(a in d.keys())
        # The result is TRUE, A is a key in our output file for the input file toy_example.txt.
        k = 'K'
        # We check that K is not a key to the output dictionary.
        self.assertFalse(k in d.keys())

        values = ['A', 'B']
        # We look to see if [A,B] are indeed values from our dictionary.
        self.assertTrue(values in d.values())
        # we want to calculate the execution time of this function for large interaction_file
        # start_time is the initialization variable of the original time
        start_time = time.time()
        large_interaction_file = "Human_HighQuality.txt"
        # we execute our function with a large interaction file
        d = TP1_Read_interaction_Graphe.read_interaction_file_dict(large_interaction_file)
        # we display the execution time of the commands included in the start time and the final time
        # we calculate the difference we have our execution time
        print("\n The execution time for the read_interaction_file_dict function is  %s seconds " % (
                    time.time() - start_time))

    def test_read_interac_list(self):
        """This test allows you to see how read_interaction_file_list works."""
        file = "file_to_clean.txt"
        t = type(TP1_Read_interaction_Graphe.read_interaction_file_list(file))
        self.assertEqual(t, list)
        l = TP1_Read_interaction_Graphe.read_interaction_file_list(file)
        couple = ('D', 'E')
        self.assertTrue(couple in l)
        false_couple = ('K', 'J')
        self.assertFalse(false_couple in l)
        # we want to calculate the execution time of this function for large interaction_file
        # start_time is the initialization variable of the original time
        start_time = time.time()
        large_interaction_file = "Human_HighQuality.txt"
        # we execute our function with a large interaction file
        l = TP1_Read_interaction_Graphe.read_interaction_file_list(large_interaction_file)
        # we display the execution time of the commands included in the start time and the final time
        # we calculate the difference we have our execution time
        print("\n The execution time for the read_interaction_file_list function is  %s seconds " % (
                    time.time() - start_time))

    def test_read_interac_global(self):
        file = "file_to_clean.txt"
        # stores the returned type in a variable
        t = type(TP1_Read_interaction_Graphe.read_interaction_file(file))
        # we verify that the  output element is a list
        self.assertEqual(t, list)
        # we store the result of this function in a variable l2
        l2 = TP1_Read_interaction_Graphe.read_interaction_file(file)
        # we verify that the second output element is a list
        self.assertEqual(type(l2[1]), list)
        # we verify that the first output element is a dict
        self.assertEqual(type(l2[0]), dict)

    def test_count_vertices(self):
        file = "file_to_clean.txt"
        count_vertices_type = type(TP1_Read_interaction_Graphe.count_vertices(file))
        self.assertEqual(count_vertices_type, int)

    def test_count_edges(self):
        file = "file_to_clean.txt"
        count_edges_type = type(TP1_Read_interaction_Graphe.count_edges(file))
        self.assertEqual(count_edges_type, int)

    def test_get_degree(self):
        file = "file_to_clean.txt"
        get_degree_type = type(TP1_Read_interaction_Graphe.get_degree(file, 'A'))
        self.assertEqual(get_degree_type, int)

    def test_get_all_degrees(self):
        file = "file_to_clean.txt"
        get_all_degrees_type = type(TP1_Read_interaction_Graphe.get_all_degrees(file))
        self.assertEqual(get_all_degrees_type, list)

    def test_get_max_degree(self):
        file = "file_to_clean.txt"
        get_degree_type = type(TP1_Read_interaction_Graphe.get_max_degree(file))
        self.assertEqual(get_degree_type, int)

    def test_get_average_degree(self):
        file = "file_to_clean.txt"
        get_average_type = type(TP1_Read_interaction_Graphe.get_average_degree(file))
        self.assertEqual(get_average_type, int)

    def test_count_degree(self):
        file = "file_to_clean.txt"
        count_degree_type = type(TP1_Read_interaction_Graphe.count_degree(file, 5))
        self.assertEqual(count_degree_type, int)

    #def test_histogram_degree(self):
        #file = "toy_example.txt"
        #histogram_type = type(ProjeyPython.histogram_degree(file))
        #self.assertEqual(histogram_type, dict)


# we notice that the read_interaction_file_list function has a longer execution time than the read_interaction_dict function.
# In order not to degrade the performance of the read_interaction function 
# we advise to read large interaction graph files by using the read_interaction_file_dict function.
# Moreover the read_interaction_file_list function requires more memory resources for large interaction files.
unittest.main()

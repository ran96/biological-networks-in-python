#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 06:56:56 2020

@author: VivianRobin
"""
from statistics import mean
import os
import imghdr
import sys


def read_interaction_file_dict(interaction_file):
    """
    

    Parameters
    ----------
    interaction_file : TextIOWrapper
    This function reads an interaction graph file and returns a dictionary. 
    In this dictionary each vertex of the interaction graph and a key, 
    the neighbours of these vertices are the values associated with the keys.
    This function needs an interaction graph file Ex.read_interaction_file_dict("toy_example.txt") 

    Returns
    
    -------
    This function returns a dictionary representing the interaction graph.

    """

    with open(interaction_file, "r") as graphe_interaction_file_TextIOWrapper:
        # creation of an empty dictionary
        dico_dict = {}
        # we read the first line of the graph file which corresponds to the number of interactions
        nb_interaction_str = graphe_interaction_file_TextIOWrapper.readline()
        # we read each line one at a time
        for ligne_str in graphe_interaction_file_TextIOWrapper.readlines():
            # Each line is composed of two proteins. We separate the lines according to the character space.
            element_list = ligne_str.split()
            # prot1 corresponds to the first protein of the line
            prot1 = element_list[0]
            # prot2 corresponds to the second protein of the line
            prot2 = element_list[1]
            # if prot1 is not a key
            if prot1 not in dico_dict.keys():
                # prot1 becomes a key
                dico_dict[prot1] = []
                # the second element of the line the value corresponding to the key
                dico_dict[prot1].append(prot2)
            # otherwise if prot1 is already a key
            else:
                # the second element of the line the value corresponding to the key
                dico_dict[prot1].append(prot2)
            # if prot2 is not a key
            if prot2 not in dico_dict.keys():
                # prot2 becomes a key
                dico_dict[prot2] = []
                # the second element of the line the value corresponding to the key
                dico_dict[prot2].append(prot1)
                # otherwise if prot2 is already a key
            else:
                # the second element of the line the value corresponding to the key
                dico_dict[prot2].append(prot1)
                # the dictionary is returned
    # print(dico_dict)
    return dico_dict


def read_interaction_file_list(interaction_file):
    """
    

    Parameters
    ----------
    interaction_file : TextIOWrapper
        This function reads an interaction graph file. 
    The output is a list of couples(x,y ) where x represents the first protein of the line 
    and y the second protein of the line.

    Returns
    -------
    This function returns a interaction list representing the interaction graph.

    """
    with open(interaction_file, "r") as graphe_interaction_file_TextIOWrapper:
        interaction_couple_list = []
        # we read the first line of the graph file which corresponds to the number of interactions
        nb_interaction_str = graphe_interaction_file_TextIOWrapper.readline()
        for ligne_str in graphe_interaction_file_TextIOWrapper.readlines():
            # Each line is composed of two proteins. We separate the lines according to the character space.
            element_list = ligne_str.split()
            # each line create a couple
            interaction_couple_list.append((element_list[0], element_list[1]))
        print(interaction_couple_list)
        # we return a list of each couple
        return interaction_couple_list


def read_interaction_file(interaction_file):
    """
    

    Parameters
    ----------
    interaction_file : TextIOWrapper
    This function takes an interaction graph file as input and returns a pair (d_int,l_int) 
    whose first element is the dictionary the graph 
    and the second the list of interactions representing the same graph. 

    Returns
    -------
    This function returns a list composed of two elements : the first one is the dictionary representing the interaction graph 
    and the second one is a list of all the interactions representing the interaction graph file.

    """
    # in order to retrieve the dictionnarent representing the graph we execute the funcion read_interaction_file_dict
    # then store the dictionary in the variable d_int
    d_int = read_interaction_file_dict(interaction_file)
    # Same for the lists but with the function :read_interaction_file_list
    l_int = read_interaction_file_list(interaction_file)
    # we create our couple where the first element is the dictionary and the second is the list.
    couple = [d_int, l_int]
    # we return our couple
    return couple


def nb_ligne(interaction_file):
    """
    

    Parameters
    ----------
    interaction_file : TextIOWrapper
        This function counts the number of lines in the interactions file.

    Returns
    -------
    the total number of line for file_interaction

    """
    # open interaction file
    fd = open(interaction_file, 'r')
    # initialization of counter
    n = 0
    # you go through each line
    for line in fd:
        # the counter is incremented by 1 for each new line
        n += 1
        # we return the counter from which we subtract one to have that the number of interactions
    return n - 1


def is_interaction_file(interaction_file):
    """
     Parameters
    ----------
    interaction_file : TextIOWrapper
        this function makes it possible to see if our input file is a good interaction file

    Returns
    -------
    te result answering the question is an interaction file? 
    if the answer is false it is not an interaction file 
    otherwise it is an interaction file so TRUE
    """
    # we test if the file is empty :
    if os.path.getsize(interaction_file) == 0:
        print(False)  # if the file is empty, we print that it is not an interaction file  stop the program.
        sys.exit()  # stop the program.
    # we test if the input file is not an image
    if (imghdr.what(interaction_file) == 'rgb' or imghdr.what(interaction_file) == 'gif' or
            imghdr.what(interaction_file) == 'pbm' or imghdr.what(interaction_file) == 'pgm' or
            imghdr.what(interaction_file) == 'ppm' or imghdr.what(interaction_file) == 'tiff' or
            imghdr.what(interaction_file) == 'rast' or imghdr.what(interaction_file) == 'xbm' or
            imghdr.what(interaction_file) == 'jpeg' or imghdr.what(interaction_file) == 'bmp' or
            imghdr.what(interaction_file) == 'png'):
        print(False)  # if the input file is pictures we print that it is not an interaction file
        sys.exit()  # stop the program.
    # open the file representing the interactions graph
    with open(interaction_file, "r") as graphe_interaction_file_TextIOWrapper:
        # the first line is the interation number
        nb_interaction_str = graphe_interaction_file_TextIOWrapper.readline()
        # we compare if the interaction number is the same as the line number
        try:  # we test whether the conversion of the first line of interaction is possible
            nb_interaction_int = int(nb_interaction_str)
            if (nb_ligne(interaction_file) != nb_interaction_int):
                print(False)  # if this number is different it is not a file representing an interaction graph.
                sys.exit()  # stop the program
        # if conversion is not possible
        except:
            print(
                False)  # the file does not contain the first line the number of interactions it is not an interaction file
            sys.exit()  # stop the program
        else:
            for ligne_str in graphe_interaction_file_TextIOWrapper.readlines():
                # Each line is composed of two proteins.
                # We separate the lines according to the character space.
                element_list = ligne_str.split()
                # we check that the file has the right number of columns, i.e. two columns for each line.
                if (len(element_list) > 2 or len(element_list) < 2):
                    print(
                        False)  # if a line has more than 2 columns or less than 2 columns, it is not an interaction file.
                    sys.exit()
    print(True)  # the input file is indeed an interaction file


""" function count_vertices
    this function counts the number of vertices in a graph
    returns the number of vertices
    """


def count_vertices(file):
    # call the function that gives a dictionary of the interactions and store its result in a variable
    dict_interaction = read_interaction_file_dict(file)
    # count the number of keys in the dictionary
    int_numb_vertices = len(dict_interaction.keys())
    return int_numb_vertices


""" function count_edges
    this function counts the number of edges in a graph by counting the elements of a list of interactions
    returns the number of edges
    """


def count_edges(file):
    # call the function that gives a list of the interactions and store its result in a variable
    list_interaction = read_interaction_file_list(file)
    # count the number of elements in the list
    int_numb_edges = len(list_interaction)
    return int_numb_edges


""" function clean_interactome
    it cleans a file of interactions from the redundant interactions and the homodimer interactions
    returns a clean file

"""
"""
# first method : this function does not work because the loop goes through the file just once
# should take into consideration the last line and how to browse the file (not only one time)
def clean_interactome(file):
    # open the file to clean
    with open(file, "r") as graphe_interaction_file_TextIOWrapper:
        # store the first line in a variable
        str_first_line = graphe_interaction_file_TextIOWrapper.readline()
        # initialize a counter to count the lines of the new file
        count = 0
        # create a list to store the lines that are good
        list_lines = []
        # browse the file to clean
        for index, str_line in enumerate(graphe_interaction_file_TextIOWrapper):
            # store the index of the next line in a variable
            int_index_next_line = index + 1
            str_next_line = graphe_interaction_file_TextIOWrapper.readlines()[int_index_next_line]
            # split the current line
            list_word = str_line.split()
            # split the next line
            list_word2 = str_next_line.split()
            # check that the current line doesn't contain an homodimer and that the next line is not an inversion of the current line
            if list_word[0] != list_word[1] and (list_word2[0] != list_word[1] or list_word2[1] != list_word2[0]):
                # cleaned_file.write(str_line + "\n")
                list_lines.append(str_line)
                count += 1
        # create the new file
        cleaned_file = open('new_interaction_file', 'w')
        # write the first line that contains the number of interactions
        cleaned_file.write(str(count) + "\n")
        # fill the new file with the lines that fit the conditions
        print(list_lines)
        for str_element in list_lines :
            cleaned_file.write(str_element[0] + str_element[1] + "\n")
        # close the new file
        cleaned_file.close()
    return cleaned_file
"""


# second method : using the type tuple for the element of the list

def clean_interactome(file):
    # open the file to clean
    with open(file, "r") as graphe_interaction_file_TextIOWrapper:
        # skip the first line
        graphe_interaction_file_TextIOWrapper.readline()
        # initialize a counter to count the lines of the new file
        count = 0
        # create a list to store the lines that are good
        list_lines = []
        # browse the file to clean
        for str_line in graphe_interaction_file_TextIOWrapper:
            list_word = str_line.split()
            if tuple(list_word) and tuple(list_word[::-1]) not in list_lines:
                if list_word[0] != list_word[1]:
                    list_lines.append(tuple(list_word))
                    count += 1
        # open the new file
        cleaned_file = open('new_interaction_file', 'w')
        # write the first line that contains the number of interactions
        cleaned_file.write(str(count) + "\n")
        # fill the new file with the lines that fit the conditions
        for element in list_lines:
            str_prot1 = element[0]
            str_prot2 = element[1]
            cleaned_file.write(str_prot1 + ' ' + str_prot2 + "\n")
        cleaned_file.close()
    return cleaned_file


""" 
    function get_degree allows to compute the degree of a protein in a graphe
    arg:
     file, protein
    returns an int : the degree of a protein
"""


def get_degree(file, prot):
    #  call the function read_interaction_dict and store its result in a variable
    interaction_dict = read_interaction_file_dict(file)
    # initialize the variable degree_int
    degree_int = 0
    # check if the protein exist in the dictionary
    if prot in interaction_dict.keys():
        # compute the length of the value of the protein
        degree_int = len(interaction_dict[prot])
    return degree_int


"""function get_all_degrees gives a list containing the degrees of each protein in the graph to use it in other 
    functions 
    arg take as argument the file to read (graph) returns a list """


def get_all_degrees(file):
    #  call the function read_interaction_dict and store its result in a variable
    interaction_dict = read_interaction_file_dict(file)
    # initialize the list of degrees
    list_of_degrees = []
    # browse the keys in the dictionary
    for protein in interaction_dict.keys():
        # retrieve the length of the value of every key
        length_of_value_int = len(interaction_dict[protein])
        # add the length to the list of lengths(degrees)
        list_of_degrees.append(length_of_value_int)
    return list_of_degrees


"""function get_max_degree(file) computes the degrees of every protein and returns the maximum degree
    arg the file to read
    returns int : the max degree
"""


def get_max_degree(file):
    # call the function get_all_degrees and store its result in a variable
    degrees_list = get_all_degrees(file)
    # use the built in function max to get the maximum number in the list
    max_degree_int = max(degrees_list)
    return max_degree_int


"""function get_average_degree computes the average degree of a graph
    arg the file to read
    returns int the average degree
"""


def get_average_degree(file):
    # call the function get_all_degrees and store its result in a variable
    degrees_list = get_all_degrees(file)
    # compute the mean of the list and add int to print a integer (without digits)
    average_degree_int = int(mean(degrees_list))
    return average_degree_int


"""function count_degree allows to find in a dictionary the number of proteins that have the same degree as the one passed in argument
    arg the file to read and the degree
    returns int: the number of proteins
"""


def count_degree(file, deg):
    # call the function get_all_degrees to have a list of all degrees in our dictionary
    list_of_degrees = get_all_degrees(file)
    # store the number of proteins that have the same degree in a variable
    num_of_proteins_int = list_of_degrees.count(deg)
    return num_of_proteins_int


"""the function histogram_degree 
"""


def histogram_degree(file, dmin, dmax):
    # create the dictionary that contains the degree as key and the number protein having this degree as value
    dict_degree_prot = {}
    # initialize the list that will contain the number of proteins for each degree between dmin and dmax
    list_proteins = []
    # loop to count the number of proteins of each degree
    for degree in range(dmin, (dmax + 1)):
        # count the proteins that have the same degree for each degree between dmin and dmax
        list_proteins.append((count_degree(file, degree)))
    # loop for to fill the dictionary with the degrees and the number of proteins
    for deg, prot in zip(range(dmin, (dmax + 1)), list_proteins):
        dict_degree_prot[deg] = prot
    # display the graph
    for degree in dict_degree_prot.keys():
        print(str(degree) + ' ' + ('*' * dict_degree_prot[degree]))
    return


# print(read_interaction_file_dict("new_interaction_file"))
# print(read_interaction_file_list("toy_example.txt"))
# print(read_interaction_file("toy_example.txt"))
# print(is_interaction_file("toy_example.txt"))
# print(count_vertices('toy_example.txt'))
# print(count_edges('toy_example.txt'))
# print(clean_interactome('toy_example.txt'))
# print(get_degree('new_interaction_file', 'Z'))
# print(get_max_degree('toy_example.txt'))
# print(get_average_degree('toy_example.txt'))
# print(count_degree('new_interaction_file', 6))
# print(get_all_degrees('new_interaction_file'))
print(histogram_degree('new_interaction_file', 1, 6))

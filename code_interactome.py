from statistics import mean
import os
import self as self
import urllib.request
import shutil
import tempfile
import re
import ssl
import code_proteome
import matplotlib.pyplot as plt
import pickle


class Interactome:
    # Attributes
    int_list = []
    int_dict = {}
    proteins = []
    neigh_id_dict = {}

    # Constructor
    def __init__(self, file, file_proteome):
        self.int_list = self.read_interaction_file_list(file)
        self.int_dict = self.read_interaction_file_dict(file)
        self.proteins = self.proteins_list(file)
        self.neigh_id_dict = self.xlink_uniprot2(file_proteome)

    # Methods

    def read_interaction_file_dict(self, file):
        """
        convert a graph (tabulated file) into a dictionary, each key(vertices) has as value the proteins that
        interacts with it
        :param file:
        :return: dictionary containing as key a protein and as a value the proteins that intercats with it
        """
        # read the file
        with open(file, "r") as interactome_file:
            # initialize the dictionary
            interaction_dict = {}
            # skip the first line
            interactome_file.readline()
            # loop for to browse the lines of the file
            for line_str in interactome_file:
                # separate the line into a list containing as elements each word of the line which are separated
                # by tabs
                word_list = line_str.split()
                # make sure that the line does not exceed 2 elements (words)
                if len(word_list) == 2:
                    # check if the key already exists in the dictionary
                    if word_list[0] in interaction_dict.keys():
                        # add the value to the existing key
                        interaction_dict[word_list[0]].append(word_list[1])
                    else:
                        # otherwise create a new key with its associated value
                        interaction_dict[word_list[0]] = [word_list[1]]
                    # check if the value is a vertex in our dict
                    if word_list[1] in interaction_dict.keys():
                        interaction_dict[word_list[1]].append(word_list[0])
                    else:
                        # otherwise create a new key with its associated value
                        interaction_dict[word_list[1]] = [word_list[0]]
        return interaction_dict

    def read_interaction_file_list(self, file):
        """
        allows to read a tabulated file and store the two elements of each line in a list by skipping the first line
        :param file:
        :return: list of interactions contained in our interactome
        """
        # read the file
        with open(file, "r") as interaction_file:
            # initialize the list
            interaction_list = []
            # skip the first line
            interaction_file.readline()
            # loop for to browse the lines of the file
            for line_str in interaction_file:
                # separate the line into a list containing as elements each word of the line which are separated by tabs
                word_list = line_str.split()
                # make sure that the line does not exceed 2 elements (words)
                if len(word_list) == 2:
                    # add the protein couple to the list
                    interaction_list = interaction_list + [(word_list[0], word_list[1])]
        return interaction_list

    def proteins_list(self, file):
        """
        This method gives us a list of the proteins present in our interactome
        :param file:
        :return: list containing the proteins
        """
        # initialize the list
        proteins_list = []
        # retrieve the proteins from the dict graph
        for prot in self.int_dict.keys():
            proteins_list.append(prot)
        return proteins_list

    def read_interaction_file(self):
        """
        Allows to store the attributes (dict and list) of an object interactome in a tuple
        :return: list
        """
        dict_list = [(self.int_dict, self.int_list)]
        return dict_list

    def is_interaction_file(self, file):
        """
        Reads a tabulated file and check if it respects some conditions by performing several tests
        :param file:
        :return: true if all the tests are good or false if the file doesn't answer the tests
        """
        # read the file
        with open(file, "r") as f:
            # store the first line in a variable
            first_line_str = f.readline()
            first_line_str = first_line_str.replace("\n", "")
            # check that the first line (and file) is not empty
            if first_line_str != "":
                # counter to count the number of lines in the file
                total_lines_int = 0
                # loop for to browse the lines of the file
                for line_str in f:
                    # increment the counter by 1
                    total_lines_int += 1
                    # separate the line into a list containing as elements each word of the line which are separated
                    # by tabs
                    word_list = line_str.split()
                    # check if the line contains only 2 elements (words)
                    if len(word_list) == 2:
                        answer_str = 'True'
                    else:
                        answer_str = 'False'
                # check that the first line contains the number of interactions in the file
                if first_line_str == str(total_lines_int):
                    answer_str = 'True'
                else:
                    answer_str = 'False'
            else:
                answer_str = 'False'
            return answer_str

    ###################week3####################################

    def count_vertices(self):
        """
        This method counts the number of vertices present in our graph
        :return: int the number of vertices
        """
        # compute the number of vertices by counting the keys of the dictionary
        int_numb_vertices = len(self.int_dict.keys())
        return int_numb_vertices

    def count_edges(self):
        """
        Counts the number of edges in a graph by counting the elements of the list of interactions
        :return: the number of edges
        """
        # compute the number of edges by counting the number of elements in the list
        int_numb_edges = len(self.int_list)
        return int_numb_edges

    def clean_interactome(self):
        """
        Cleans a file of interactions from the redundant interactions and the homodimer interactions
        :return: a clean file
        """
        # create a list to store the interactions that are good
        interactions_list = []
        count = 0
        # open the file to clean
        for interaction_tuple in self.int_list:
            if interaction_tuple and interaction_tuple[::-1] not in interactions_list:
                if interaction_tuple[0] != interaction_tuple[1]:
                    interactions_list.append(interaction_tuple)
                    count += 1
            # open the new file
            cleaned_file = open('new_interaction_file', 'w')
            # write the first line that contains the number of interactions
            cleaned_file.write(str(count) + "\n")
            # fill the new file with the lines that fit the conditions
            for element in interactions_list:
                str_prot1 = element[0]
                str_prot2 = element[1]
                cleaned_file.write(str_prot1 + ' ' + str_prot2 + "\n")
            cleaned_file.close()
        return cleaned_file

    def get_degree(self, prot):
        """
        Computes the degree of a protein in a graph
        :param prot:
        :return: int : the degree of the protein given as a parameter
        """
        # initialize the variable degree_int
        degree_int = 0
        # check if the protein exist in the dictionary
        if prot in self.int_dict.keys():
            # compute the length of the value (neighbours) of the protein
            degree_int = len(self.int_dict[prot])
        return degree_int

    def get_all_degrees(self):
        """
        Creates a list containing the degrees of each protein in the graph to use it in other
        functions
        :return: list
        """
        # initialize the list of degrees
        degress_list = []
        # browse the keys in the dictionary
        for protein in self.int_dict.keys():
            # retrieve the length of the value of each key
            length_of_value_int = len(self.int_dict[protein])
            # add the length to the list of lengths(degrees)
            degress_list.append(length_of_value_int)
        return degress_list

    def get_max_degree(self):
        """
        Computes the degrees of every protein in the graph and returns the maximum degree
        :return: int : the max degree
        """
        # call the function get_all_degrees and store its result in a variable
        degrees_list = self.get_all_degrees()
        # use the built in function max to get the maximum number in the list
        max_degree_int = max(degrees_list)
        return max_degree_int

    def get_average_degree(self):
        """
        Computes the average degree of a graph
        :return: int the average degree
        """
        # call the function get_all_degrees and store its result in a variable
        degrees_list = self.get_all_degrees()
        # compute the mean of the list and add int to print an integer (without digits)
        average_degree_int = int(mean(degrees_list))
        return average_degree_int

    def count_degree(self, deg):
        """
        Finds in a dictionary the number of proteins that have the same degree as the one passed in argument
        :param deg:
        :return: int: the number of proteins
        """
        # call the function get_all_degrees to have a list of all degrees in our dictionary
        list_of_degrees = self.get_all_degrees()
        # store the number of proteins that have the same degree in a variable
        num_of_proteins_int = list_of_degrees.count(deg)
        return num_of_proteins_int

    def histogram_degree(self, dmin, dmax):
        """
        counts the number of proteins having a degree between dmin and dmax
        :param dmin:
        :param dmax:
        :return: the result as an histogram
        """
        # create the dictionary that contains the degree as key and the number protein having this degree as value
        dict_degree_prot = {}
        # initialize the list that will contain the number of proteins for each degree between dmin and dmax
        list_proteins = []
        # loop to count the number of proteins of each degree
        for degree in range(dmin, (dmax + 1)):
            prot_degree_int = self.count_degree(degree)
            # count the proteins that have the same degree for each degree between dmin and dmax
            list_proteins.append(prot_degree_int)
        # loop for to fill the dictionary with the degrees and the number of proteins
        for deg, prot in zip(range(dmin, (dmax + 1)), list_proteins):
            dict_degree_prot[deg] = prot
        # display the graph
        for degree in dict_degree_prot.keys():
            print(str(degree) + ' ' + ('*' * dict_degree_prot[degree]))
        return

    #####################week 4############################################

    def cc_dict(self):
        """
        This method creates a dictionary containing the connected components of a graph
        :return: dictionary
        """

        # implement the dfs algorithm
        def dfs(v):
            # add the vertices to the list visited to mark them
            visited.append(v)
            # check the neighbours of the vertices
            for nei in self.int_dict[v]:
                # check that the neighbour is not in visited
                if nei not in visited:
                    # add the neighbours as values to the dictionary of connected component
                    cc_dict[node].append(nei)
                    dfs(nei)

        # initialize a list to store in it the visited vertices
        visited = []
        # create a dictionary of connected components
        cc_dict = {}
        # browse through all the vertices of the graph
        for node in self.proteins:
            # add the vertices to the list visited
            if node not in visited:
                # add the node(the first vertices in a connected component) as a key to the dictionary
                cc_dict[node] = []
                # apply the algorithm dfs to each vertices
                dfs(node)
        return cc_dict

    def dict_to_list(self):
        """
        Convert the dictionary of the connected components into a list to facilitate the other methods
        :return: list
        """
        cc_list = []
        # call the function cc_dict to have the list of connect component of the graph
        cc_dict = self.cc_dict()
        # browsing the keys of the dict and adding it to the list
        for key in cc_dict.keys():
            # create a temporary list containing the elements of the connected component
            temp_list = [key]
            for ele in cc_dict[key]:
                temp_list.append(ele)
            # add the list of the cc to the general list containing all the cc
            cc_list.append(temp_list)
        return cc_list

    def countcc(self):
        """
        Counts all the connected component of a graph and give the length of each one of them
        :return: output_str
        """
        # call the function dict_to_list
        list_cc = self.dict_to_list()
        # display the total number of all the connect component
        output_str = print("On a", str(len(list_cc)), "composantes connexes dans ce graphe")
        count_int = 0
        # display the number and the length of each connect component
        for cc in list_cc:
            print("la composante connexe numéro", str(count_int + 1), "est composée de ", str(len(cc)), "protéines")
            count_int += 1
        return output_str

    def writecc(self):
        """
        Creation a new file and fill each line with the cc of a graph, each line begins with the length of the cc
        :return: file
        """
        # call the function dict to list to have the list of connect component of the graph
        list_cc = self.dict_to_list()
        # open the new file
        new_file = open('cc_file', 'w')
        # fill the file with a connected component list in each line
        for cc in list_cc:
            length_int = len(cc)
            new_file.write(str(length_int) + " " + str(cc) + "\n")
        new_file.close()
        return new_file

    def extractcc(self, prot):
        """
        Displays the connected component that contains the prot passed in argument
        :param prot:
        :return: list of the cc
        """
        # call the function cc_dict to have the list of connected components of the graph
        cc_list = self.dict_to_list()
        # browse the list of connect component
        for cc in cc_list:
            # look for the connect component that contains the prot passed in argument
            if prot in cc:
                list_vertices = cc
        return list_vertices

    def extract_cc_number(self, prot):
        """
        Computes the number of the connected components where the protein belongs
        :param prot:
        :return: count_int
        """
        # call the function cc_dict to have the list of connect component of the graph
        cc_list = self.dict_to_list()
        # initialize the counter
        count_int = 0
        # browse the list of connect component
        for cc in cc_list:
            count_int += 1
            # look for the connected component that contains the prot passed in argument
            if prot in cc:
                # print("la protéine appartient à la composante connexe numéro", str(count_int))
                break
        return count_int

    def compute_cc(self):
        """
        Compute a list in which each element corresponds to the related component number of the protein at position i
        in the protein list of the graph
        :return: list
        """
        # initialize the list that will contain the related connect component number of each protein
        cc_prot_number_list = []
        # browse the proteins list to retrieve the number of the connect component they belong too
        # by using the extractcc_number method
        for prot in self.proteins:
            cc_prot_number_list.append(self.extract_cc_number(prot))
        return cc_prot_number_list

    def density(self):
        """
        Computes the density of a non oriented graph
        :return: dens:int
        """
        # computing the number of vertices
        length_dict_int = len(self.int_dict.keys())
        # compute the number of edges
        length_edges_int = len(self.int_list)
        # density_int = int((length_dict_int * (length_dict_int - 1)) / 2)
        # search for which one is good
        num = int(2 * length_edges_int)
        den = int(length_dict_int * (length_dict_int - 1))
        dens = num / den
        return round(dens, 2)

    def clustering(self, prot):
        """
        gives the clustering coefficient of the protein passed in argument
        :param prot:
        :return: cluster_coeff : float
        """
        # initializing the number of triangles
        triangle_int = 0
        # initialize a list to avoid counting interaction twice
        interaction_list = []
        # distinct neighbours of the prot
        distinct_neighbours_int = int((self.get_degree(prot) * (self.get_degree(prot) - 1)) / 2)
        # counting the number of triangles linked to the protein passed in argument
        if self.get_degree(prot) > 1:
            # double loop to search if there is a link between the neighbours of the prot
            for neighbour_1 in self.int_dict[prot]:
                for neighbour_2 in self.int_dict[prot]:
                    # check that there is a connexion between the neighbours
                    if (neighbour_1, neighbour_2) or (neighbour_2, neighbour_1) in self.int_list and (
                            (neighbour_1, neighbour_2) and (neighbour_2, neighbour_1)) not in interaction_list:
                        triangle_int += 1
                        interaction_list.append([(neighbour_1, neighbour_2), (neighbour_2, neighbour_1)])
            cluster_coef_float = float(triangle_int / distinct_neighbours_int)
        else:
            print('prot has one or no neighbour')
            cluster_coef_float = 0
        return cluster_coef_float

    #################################week 5###########################################

    def xlink_uniprot(self, file):
        """
        this method stores the neighbours and the uniprot ID of a protein in a nested dictionary
        :parameter: file: uniprot file
        :return: nested dictionary
        """
        # create an instance proteome
        objet_proteome = code_proteome.Proteome(file)
        # initialize the dictionary
        neigh_uniprot_id_dict = {}
        # store the dict with pickle
        with open("neigh_uniprot_id_dict.pkl", "wb") as pklFile:
            # add for every prot a dictionary containing the neighbours and the uniprotID as keys and values
            for prot in self.int_dict.keys():
                if prot not in objet_proteome.prot_ID_dict.keys():
                    neigh_uniprot_id_dict[prot] = {'neighbours': self.int_dict[prot], 'uniprotID': "no uniprot id"}
                else:
                    neigh_uniprot_id_dict[prot] = {'neighbours': self.int_dict[prot],
                                                   'uniprotID': objet_proteome.prot_ID_dict[prot]}
            pickle.dump(neigh_uniprot_id_dict, pklFile)
        return neigh_uniprot_id_dict

    # version ou on garde les prot sans identifient uniprot

    # version ou on les garde pas
    def xlink_uniprot2(self, file):
        """
        this method stores the neighbours and the uniprot ID of a protein in a nested dictionary
        :parameter: file: uniprot file
        :return: nested dictionary
        """
        # create an instance proteome
        objet_proteome = code_proteome.Proteome(file)
        # initialize the dictionary
        neigh_uniprot_id_dict = {}
        # store the dict with pickle
        with open("neigh_uniprot_id_dict.pkl", "wb") as pklFile:
            # add for every prot(key) a dictionary containing the neighbours and the uniprotID as keys and values
            for prot in self.int_dict.keys():
                for key in objet_proteome.prot_ID_dict.keys():
                    if key == prot:
                        neigh_uniprot_id_dict[prot] = {'neighbours': self.int_dict[prot],
                                                       'uniprotID': objet_proteome.prot_ID_dict[key]}
            pickle.dump(neigh_uniprot_id_dict, pklFile)
        return neigh_uniprot_id_dict

    def get_protein_domains(self, prot):
        """
        this method retrieves the domains of a protein from its uniprot page
        :param prot:
        :return: dictionary containing the prot as  key and a dictionary of domains with their occurrences as value
        """
        # initialize the dictionary
        domains_dict = {}
        # get access to the url page
        with urllib.request.urlopen('https://www.uniprot.org/uniprot/' + str(prot) + '.txt') as response:
            # open a temporary file
            with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                # copy the url page into the temp file
                shutil.copyfileobj(response, tmp_file)
        # read the temp file to get info () '  DR\s+Pfam;\sPF[0-9]{5};[\w\-_];\s[0-9]
        with open(tmp_file.name) as html:
            uniprot_page = html.read()
            # using regular expressions to search for the lines containing the domains of the protein
            domain_lines_list = re.findall(r'DR\s{3}Pfam;\s[A-Z\d]+;\s[\dA-Za-z_-]+;\s[0-9]+', uniprot_page)  # [\d+]
            # initialize the dictionary of domains
            domains_list = {}
            # print(domain_lines_list)
            # browse the lines retrieved from the file uniprot (temp file)
            for domain in domain_lines_list:
                # separate the lines into a list
                str_list = domain.split('; ')
                # print(str_list)
                # add the field containing the domains to the dict as key and also the number of times a domain
                # occurs in the protein as value
                domains_list[str_list[2]] = str_list[3]
                # domains_list.extend(new_list[2:4]) ==> to add the occurrence of the domain
            # nested dictionary
            domains_dict[prot] = domains_list
        return domains_dict

    def xlinkdomains(self):
        """
        this method extend the nested dictionary by adding the key 'domains' to the uniprot ID and neighbours keys
        :return: nested dictionary
        """
        with open("neigh_uniprot_id_dict.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
            # browse the proteins of our graph to get domains for each one
            count = 0
            # initialize a list to store the proteins whose domains could not be collected
            prot_problem_list = []
            for p in neigh_id_dom_dict.keys():
                count += 1
                print('Prot', p, 'numero', count)
                # get the domains of every prot
                try:
                    prot_domains_dict = self.get_protein_domains(p)
                    # browse the result of get_protein_domain and add domains n times their value (number of the same
                    # domain in a protein)
                    dom_list = []
                    for prot, domain in prot_domains_dict.items():
                        for key in domain:
                            # add the domain n times
                            for i in range(int(domain[key])):
                                dom_list.append(key)
                    # add the key domains and its value to the nested dictionary
                    neigh_id_dom_dict[p]['domains'] = dom_list
                except:
                    prot_problem_list.append(p)
                    continue
                # time.sleep(1)
        # use serialization to store the generated dictionary
        with open("neigh_id_dom_dict.pkl", "wb") as pklFile:
            pickle.dump(neigh_id_dom_dict, pklFile)
        with open("liste_prot_ou_ça_bug.pkl", "wb") as pklFile:
            pickle.dump(prot_problem_list, pklFile)
        return neigh_id_dom_dict, len(prot_problem_list)  # prot_problem_list,

    def get_prot_dom_missing(self):
        """
        retrieve the domains of the proteins whose domains could not be collected
        :return:
        """
        prot_list = ['ARF4_HUMAN', 'GRM3_HUMAN', 'LDLR_HUMAN']
        with open("neigh_id_dom_dict.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
            for prot in prot_list:
                prot_domains_dict = self.get_protein_domains(prot)
                # browse the result of get_protein_domain and add domains n times their value (number of the same
                # domain in a protein)
                dom_list = []
                for p, domain in prot_domains_dict.items():
                    for key in domain:
                        # add the domain n times
                        for i in range(int(domain[key])):
                            dom_list.append(key)
                # add the key domains and its value to the nested dictionary
                neigh_id_dom_dict[prot]['domains'] = dom_list
                print(len(neigh_id_dom_dict.keys()))
            with open("prot_nei_id_dom_dict.pkl", "wb") as pklFile:
                pickle.dump(neigh_id_dom_dict, pklFile)
        return neigh_id_dom_dict['ARF4_HUMAN'], neigh_id_dom_dict['GRM3_HUMAN'], neigh_id_dom_dict['LDLR_HUMAN']

    def get_domains(self):
        """
        Displays the list of non redundant domains
        :return: list
        """
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
            dom_list = []
            for prot in final_dict.keys():
                for dom in final_dict[prot]['domains']:
                    if dom not in dom_list:
                        dom_list.append(dom)
        return len(dom_list)

    def check_prot_with_no_dom(self):
        """
        This method collect the proteins that don't have domains
        :return:
        """
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
            prot_list = []
            for prot in final_dict.keys():
                if len(final_dict[prot]['domains']) == 0:
                    prot_list.append(prot)
        return prot_list  # 590 proteines sans domaines

    ##############dictionnaire fianl = prot_nei_id_dom_dict.pkl

    ################################week 6###############################################

    def ls_proteins(self):
        """
        This method gives a list of the proteins in the graph
        :return: list containing the proteins
        """
        proteins_list = []
        for prot in self.int_dict.keys():
            proteins_list.append(prot)
        return proteins_list

    def ls_domains(self):
        """
        ls_domains : displays the non redundant domains of all the proteins contained in the graph
        :return: list
        """
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
            # initialize the list of domains
            non_redundant_domains_list = []
            # fill the list with the domains
            for prot in final_dict.keys():
                for dom in final_dict[prot]['domains']:
                    if dom not in non_redundant_domains_list:
                        non_redundant_domains_list.append(dom)
        return non_redundant_domains_list  # 3620 non redundant domains

    def ls_all_domains(self):
        """
        this method displays a list of all the domains contained in the graph
        :return: list with redundant domains
        """
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
            # initialize the list of domains
            all_domains_list = []
            # fill the list with the domains
            for prot in final_dict.keys():
                for dom in final_dict[prot]['domains']:
                    all_domains_list.append(dom)
        with open("list_of_all_domains.pkl", "wb") as pklFile:
            pickle.dump(all_domains_list, pklFile)
        return all_domains_list

    def ls_domains_n(self, n):
        """
        ls_domains_n displays the domains that are found at least n times in our graph
        :param n: number of times the domain is found
        les domaines qui apparaissent au moins n fois (dans la même prot ou dans plusieurs)
        """
        domains_list = []
        redundant_domains_list = self.ls_all_domains()
        for dom in redundant_domains_list:
            if dom not in domains_list and redundant_domains_list.count(dom) >= n:
                domains_list.append(dom)
        return domains_list

    def graph_prot_id_neigh_non_redundant_dom(self):
        """
        this method produces a graph of the proteins with only the unique domains of the protein as opposed to the
        initial graph where we had the list of all the domains of a protein.
        """
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
            # modify the initial dict by removing the domains present several times
            for prot in final_dict.keys():
                final_dict[prot]['domains'] = list(set(final_dict[prot]['domains']))
        with open("graph_prot_unique_domains.pkl", "wb") as pklFile:
            pickle.dump(final_dict, pklFile)
        return final_dict

    def check_nb_of_dom(self):
        with open("graph_prot_unique_domains.pkl", "rb") as pklFile:
            final_dict = pickle.load(pklFile)
            dom_list = []
            for prot in final_dict.keys():
                for dom in final_dict[prot]['domains']:
                    if dom not in dom_list:
                        dom_list.append(dom)
            return len(dom_list)  # check ok 3620 # use this new graph for the distribution

    def nb_domains_by_protein_distribution(self):
        """
        this method displays a dictionary containing the number of domains as key and the number of protein
        containing this number of domains as value
        :return: dictionary
        """
        domains_occurrence_list = []
        domains_occurrence_dict = {}
        # get the merged dictionary (neighbours, uniprot id, domains)
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
        # count the number of domains for each protein
        for prot in neigh_id_dom_dict.keys():
            domains_occurrence_list.append(len(neigh_id_dom_dict[prot]['domains']))
            if len(neigh_id_dom_dict[prot]['domains']) == 310:
                print(prot)
        # sort the list
        domains_occurrence_list.sort()
        # count the occurrences of proteins having the same number of domains
        for elem in domains_occurrence_list:
            if elem not in domains_occurrence_dict.keys():
                domains_occurrence_dict[elem] = domains_occurrence_list.count(elem)
        return domains_occurrence_dict

    def nb_unique_domains_by_protein_distribution2(self):
        """
        this method displays a dictionary containing the number of unique domains as key and the number of protein
        containing this number of domains as value
        :return: dictionary
        """
        domains_occurrence_list = []
        domains_occurrence_dict = {}
        # get the merged dictionary (neighbours, uniprot id, domains)
        with open("graph_prot_unique_domains.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
        # count the number of domains for each protein
        for prot in neigh_id_dom_dict.keys():
            domains_occurrence_list.append(len(neigh_id_dom_dict[prot]['domains']))
            if len(neigh_id_dom_dict[prot]['domains']) == 310:
                print(prot)
        # sort the list
        domains_occurrence_list.sort()
        # count the occurrences of proteins having the same number of domains
        for elem in domains_occurrence_list:
            if elem not in domains_occurrence_dict.keys():
                domains_occurrence_dict[elem] = domains_occurrence_list.count(elem)
        return domains_occurrence_dict

    def hist_nb_dom_by_prot_distribution(self, domains_occurrence_dict):
        """
        Method to display a barplot of the function above (number of domains by proteins distribution)
        """
        # occurrences_dict = self.nb_domains_by_protein_distribution()
        # retrieve the keys as a list
        nb_dom_list = domains_occurrence_dict.keys()
        print(nb_dom_list)
        # retrieve the values as a list
        nb_prot_list = domains_occurrence_dict.values()
        print(nb_prot_list)
        # plot the hist using the package matplotlib
        plt.bar(nb_dom_list, nb_prot_list)
        # plt.xlim(0, 50)
        plt.xlabel('Number of domains')
        plt.ylabel('Number of proteins')
        plt.title('Number of domains by proteins distribution')
        plt.show()
        # 1 proteine ayant 310 domaines,1==>76 domaines...

    def get_dom_prot_dict(self):
        """
        this method returns a dict which contains a domain as a key and the list of the proteins that contain this
        domain as a value and will help us for the next method
        :return: dictionary
        """
        # first get the merged dictionary (neighbours, uniprot id, domains)
        with open("graph_prot_unique_domains.pkl", "rb") as pklFile: # je le change pour voir si ya un chgt prot_nei_id_dom_dict
            neigh_id_dom_dict = pickle.load(pklFile)
        # initialize the dict
        dom_prot_dict = {}
        # browse the list of non redundant domains
        for dom in self.ls_domains():
            dom_prot_dict[dom] = []
            for protein, key in neigh_id_dom_dict.items():
                # check if the domain exist in this protein
                if dom in neigh_id_dom_dict[protein]['domains']:
                    dom_prot_dict[dom].append(protein)
        return dom_prot_dict

    def nb_proteins_by_domain_distribution(self):
        """
        this method displays a dictionary whose keys correspond to the number of proteins sharing the same domain and
        the values represent the sum of the domains contained in these proteins
        :return: dictionary
        """
        # search for the number of proteins that share the same domains
        # initialize the dict
        prot_dom_distribution_dict = {}
        # first get the merged dictionary (neighbours, uniprot id, domains)
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
        # get the dict of domains and list of proteins
        dom_prot_dict = self.get_dom_prot_dict()
        # get the length of the protein list
        for dom in dom_prot_dict.keys():
            nb_of_prot_int = len(dom_prot_dict[dom])
            # check if the domain is shared by several proteins
            if nb_of_prot_int > 1:
                # initialize the sum of the domains presents in all these proteins
                nb_of_dom_int = 0
                # retrieve the number of domains for each protein and add it to the rest
                for prot in dom_prot_dict[dom]:
                    nb_of_dom_int += len(neigh_id_dom_dict[prot]['domains'])
                # fill in the dict
                prot_dom_distribution_dict[nb_of_prot_int] = nb_of_dom_int
        return prot_dom_distribution_dict

    def nb_proteins_by_unique_domain_distribution2(self):
        """
        this method displays a dictionary whose keys correspond to the number of proteins sharing the same domain and
        the values represent the sum of the domains contained in these proteins
        :return: dictionary
        """
        # search for the number of proteins that share the same domains
        # initialize the dict
        prot_dom_distribution_dict = {}
        # first get the merged dictionary (neighbours, uniprot id, domains)
        with open("graph_prot_unique_domains.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
        # get the dict of domains and list of proteins
        dom_prot_dict = self.get_dom_prot_dict()
        # get the length of the protein list
        for dom in dom_prot_dict.keys():
            nb_of_prot_int = len(dom_prot_dict[dom])
            # check if the domain is shared by several proteins
            if nb_of_prot_int > 1:
                # initialize the sum of the domains presents in all these proteins
                nb_of_dom_int = 0
                # retrieve the number of domains for each protein and add it to the rest
                for prot in dom_prot_dict[dom]:
                    nb_of_dom_int += len(neigh_id_dom_dict[prot]['domains'])
                # fill in the dict
                prot_dom_distribution_dict[nb_of_prot_int] = nb_of_dom_int
        return prot_dom_distribution_dict

    def hist_nb_prot_by_dom_distribution(self, graph):
        """
        Method to display a barplot of the function above (number of domains by proteins distribution)
        """
        # prot_dom_dict = self.nb_proteins_by_domain_distribution()
        # retrieve the keys as a list
        nb_prot_list = graph.keys()
        print(nb_prot_list)
        # retrieve the values as a list
        nb_dom_list = graph.values()
        print(nb_dom_list)
        # plot the hist using the package matplotlib
        plt.bar(nb_prot_list, nb_dom_list)
        plt.xlabel('Number of proteins')
        plt.ylabel('Number of domains')
        plt.title('Number of proteins by domains distribution')
        plt.show()

    def co_occurrence(self, dom_x, dom_y):
        """
        this method counts the number of occurrences of the two domains given as parameters in our graph
        :param dom_x:
        :param dom_y:
        :return: int (number of occurrences)
        """
        nb_occurrences_int = 0
        # first get the merged dictionary (neighbours, uniprot id, domains)
        with open("prot_nei_id_dom_dict.pkl", "rb") as pklFile:
            neigh_id_dom_dict = pickle.load(pklFile)
        for prot in neigh_id_dom_dict.keys():
            # check if both domains are present in the protein
            if dom_x in neigh_id_dom_dict[prot]['domains'] and dom_y in neigh_id_dom_dict[prot]['domains']:
                nb_occurrences_int += 1
        return nb_occurrences_int


# file_inter = Interactome("fichier_vide.txt")
human = Interactome("Human_HighQuality.txt", "uniprotlist_human_high_quality.tab")
file_to_test = Interactome("fichier_test.txt", "uniprot_fichier_test.tab")

# print(file_to_test.merged_dict)
# print(file_to_test.get_protein_domains("P06213"))
# print(file_to_test.xlink_uniprot('uniprot_fichier_test.tab'))
# print(file_to_test.xlinkdomains())
# print(file_to_test.ls_domains())
# print(file_to_test.ls_proteins())
# print(file_to_test.get_protein_domains('INSR_HUMAN'))
# print(type(file_to_test.countcc()))
# print(human.int_dict)
# print(len(human.int_dict))
# print(human.xlink_uniprot2('uniprotlist_human_high_quality.tab'))
# print(human.xlinkdomains())
# print(human.test_dict_from_xlikdomains())
# print(human.get_prot_dom_missing())
# print(human.get_domains())
# print(human.get_protein_domains('APOA_HUMAN'))
# print(human.int_dict)
# print(human.hist_nb_dom_by_prot_distribution())
# print(human.hist_nb_prot_by_dom_distribution())
# print(len(human.ls_all_domains()))
# print(human.graph_prot_id_neigh_non_redundant_dom())
# print(human.check_nb_of_dom())
# dic = human.nb_unique_domains_by_protein_distribution2()
# print(human.hist_nb_dom_by_prot_distribution(dic))
dic2 = human.nb_proteins_by_domain_distribution()
print(human.hist_nb_prot_by_dom_distribution(dic2))
# print(human.check_prot_with_no_dom())
# print(len(human.ls_domains()))
# print(file_to_test.ls_domains())
# print(file_to_test.ls_all_domains())
# print(file_to_test.ls_domains_n(2))
# print(file_to_test.nb_domains_by_protein_distribution())
# print(file_to_test.hist_nb_dom_by_prot_distribution())
# print(file_to_test.get_dom_prot_dict())
# print(file_to_test.nb_proteins_by_domain_distribution())
# print(file_to_test.hist_nb_prot_by_dom_distribution())
